package mk.plugin.asbp.command;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import mk.plugin.asbp.config.Configs;
import mk.plugin.asbp.gui.UpgradeGUI;
import mk.plugin.asbp.timedfly.TimedFly;

public class AdminCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		
		try {
			
			if (args[0].equalsIgnoreCase("upgradegui")) {
				Player player = Bukkit.getPlayer(args[1]);
				UpgradeGUI.open(player);
				sender.sendMessage("§a[ASkyBlockPlus] Opened upgrade gui for " + player.getName());
			}
			
			else if (args[0].equalsIgnoreCase("settimefly")) {
				if (!Configs.F_TIMED_FLY) {
					sender.sendMessage("§c[ASkyBlockPlus] Feature disabled");
					return false;
				}
				Player p = Bukkit.getPlayer(args[1]);
				long time = Integer.valueOf(args[2]) * 1000;
				TimedFly.set(p, time);
				p.sendMessage("§aBạn đã có thể bay trong " +  time / 1000 + " giây");
				sender.sendMessage("§a[ASkyBlockPlus] Set flying for " + p.getName() + " in " + time / 1000 + "s");
				p.playSound(p.getLocation(), Sound.ENTITY_GHAST_SHOOT, 1f, 1f);
			}
			
		}
		catch (ArrayIndexOutOfBoundsException e) {
			sendHelp(sender);
		}
		
		return false;
	}
	
	public void sendHelp(CommandSender sender) {
		sender.sendMessage("");
		sender.sendMessage("/askyblockplus upgradegui <player>");
		sender.sendMessage("/askyblockplus settimefly <player> <time(seconds)>");
		sender.sendMessage("");
	}

}
