package mk.plugin.asbp.listener;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFromToEvent;

import com.wasteofplastic.askyblock.ASkyBlockAPI;
import com.wasteofplastic.askyblock.Island;

import mk.plugin.asbp.main.MainASBP;
import mk.plugin.asbp.oregen.Oregens;

public class BlockListener implements Listener {
	
	@EventHandler
	public void onBlockFromTo(BlockFromToEvent e) {
		Block block = e.getBlock();
		Location location = block.getLocation();
		Island island = ASkyBlockAPI.getInstance().getIslandAt(location);
		if (island == null) return;

		// Check
		Material material = block.getType();
		Block toBlock = e.getToBlock();
		Location toLocation = toBlock.getLocation();
		if (material.equals(Material.WATER) || material.equals(Material.LAVA)) {
			Island toIsland = ASkyBlockAPI.getInstance().getIslandAt(toLocation);
			if (island != toIsland) e.setCancelled(true);
		}
		if (!this.isSurroundedByWater(toLocation)) return;
		
		
		// Rate
		Bukkit.getScheduler().runTask(MainASBP.get(), () -> {
			Material toMaterial = toBlock.getType();
			if (toMaterial.equals(Material.COBBLESTONE) || toMaterial.equals(Material.STONE)) {
				Material result = Oregens.rate(island);
				toBlock.setType(result);
			}
		});
		
	}
	
	public boolean isSurroundedByWater(Location location) {
		World world = location.getWorld();
		if (world == null) {
			return false;
		} else {
			int x = location.getBlockX();
			int y = location.getBlockY();
			int z = location.getBlockZ();
			int[][] coords = new int[][]{{x + 1, y, z}, {x - 1, y, z}, {x, y, z + 1}, {x, y, z - 1}, {x + 1, y + 1, z},
					{x - 1, y + 1, z}, {x, y + 1, z + 1}, {x, y + 1, z - 1}, {x + 1, y - 1, z}, {x - 1, y - 1, z},
					{x, y - 1, z + 1}, {x, y - 1, z - 1}};
			int[][] var7 = coords;
			int var8 = coords.length;

			for (int var9 = 0; var9 < var8; ++var9) {
				int[] coord = var7[var9];
				Block block = world.getBlockAt(coord[0], coord[1], coord[2]);
				Material material = block.getType();
				String name = material.name();
				if (name.contains("WATER")) {
					return true;
				}
			}

			return false;
		}
	}
	
}
