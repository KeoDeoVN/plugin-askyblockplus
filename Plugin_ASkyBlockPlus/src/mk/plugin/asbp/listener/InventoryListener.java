package mk.plugin.asbp.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import mk.plugin.asbp.gui.UpgradeGUI;

public class InventoryListener implements Listener {
	
	@EventHandler
	public void onGUIClick(InventoryClickEvent e) {
		UpgradeGUI.eventClick(e);
	}
	
}
