package mk.plugin.asbp.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;

import mk.plugin.asbp.border.Borders;
import mk.plugin.asbp.config.Configs;
import mk.plugin.asbp.main.MainASBP;
import mk.plugin.asbp.timedfly.TimedFly;
import mk.plugin.asbp.upgrade.Upgrade;
import mk.plugin.asbp.utils.Utils;

public class PlayerListener implements Listener {
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		Bukkit.getScheduler().runTaskLater(MainASBP.get(), () -> {
			for (Upgrade u : Upgrade.values()) {
				u.save(player.getUniqueId());
			}
			
			// Check border
			if (Configs.F_BORDER) Borders.check(player, true);
		}, 10);
	}
	
	@EventHandler
	public void onTeleport(PlayerTeleportEvent e) {
		Player player = e.getPlayer();
		
		// Check border
		if (Configs.F_BORDER) {
			Bukkit.getScheduler().runTask(MainASBP.get(), () -> {
				Borders.check(player, true);
			});
		}
		
		// Check allow flight island
		Bukkit.getScheduler().runTask(MainASBP.get(), () -> {
			if (Utils.isMemberOfIsland(player.getLocation(), player) && TimedFly.isTrigger(player)) player.setAllowFlight(true);
		});
	}
	
	@EventHandler
	public void onToggleFlight(PlayerToggleFlightEvent e) {
		Player p = e.getPlayer();
		if (p.hasPermission("askyblockplus.bypass")) return;
		if (TimedFly.isTrigger(p)) {
			if (!Utils.isMemberOfIsland(p.getLocation(), p)) {
				p.sendMessage("§cChỉ được bay ở đảo của mình");
				Bukkit.getScheduler().runTask(MainASBP.get(), () -> {
					p.setFlying(false);
				});
			}
		}
	}
	
	
	
	
	
	
	
	

}
