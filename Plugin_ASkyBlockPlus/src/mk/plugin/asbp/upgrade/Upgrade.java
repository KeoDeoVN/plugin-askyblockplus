package mk.plugin.asbp.upgrade;

import java.util.UUID;

import org.bukkit.Bukkit;

import com.google.common.collect.Lists;

import mk.plugin.asbp.config.Configs;
import mk.plugin.asbp.main.MainASBP;
import mk.plugin.playerdata.storage.PlayerData;
import mk.plugin.playerdata.storage.PlayerDataAPI;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.Node;

public enum Upgrade {
	
	OREGEN {
		@Override
		public String getID(UUID player) {
			String name = Bukkit.getOfflinePlayer(player).getName();
			PlayerData data = PlayerDataAPI.get(name, MainASBP.get().getName());
			return data.hasData("askyblockplus-oregen") ? data.getValue("askyblockplus-oregen") : "o-0"; 
		}

		@Override
		public void save(String id, UUID player) {
			if (!Configs.oregenUpgrades.containsKey(id)) return;
			String name = Bukkit.getOfflinePlayer(player).getName();
			PlayerData data = PlayerDataAPI.get(name, MainASBP.get().getName());
			data.set("askyblockplus-oregen", id);
			data.save();
		}

		@Override
		public UData getUpgradeData(UUID player) {
			return Configs.oregenUpgrades.get(this.getID(player));
		}

		@Override
		public UData getUpgradeData(String id) {
			return Configs.oregenUpgrades.get(id);
		}
		
	},
	MEMBER {
		@Override
		public String getID(UUID player) {
			String name = Bukkit.getOfflinePlayer(player).getName();
			PlayerData data = PlayerDataAPI.get(name, MainASBP.get().getName());
			return data.hasData("askyblockplus-member") ? data.getValue("askyblockplus-member") : "m-0"; 
		}

		@Override
		public void save(String id, UUID player) {
			if (!Configs.memberUpgrades.containsKey(id)) return;
			String name = Bukkit.getOfflinePlayer(player).getName();
			PlayerData data = PlayerDataAPI.get(name, MainASBP.get().getName());
			data.set("askyblockplus-member", id);
			data.save();
			
			// Luckperms
			User user = LuckPermsProvider.get().getUserManager().getUser(player);
			for (Node node : Lists.newArrayList(user.getNodes())) {
				if (node.getKey().contains("askyblock.team.maxsize."))
				user.data().remove(Node.builder(node.getKey()).build());
			}
			user.data().add(Node.builder("askyblock.team.maxsize." + Configs.memberUpgrades.get(id).getMember()).build());
			LuckPermsProvider.get().getUserManager().saveUser(user);
		}

		@Override
		public UData getUpgradeData(UUID player) {
			return Configs.memberUpgrades.get(this.getID(player));
		}

		@Override
		public UData getUpgradeData(String id) {
			return Configs.memberUpgrades.get(id);
		}
		
	},
	SIZE {
		@Override
		public String getID(UUID player) {
			String name = Bukkit.getOfflinePlayer(player).getName();
			PlayerData data = PlayerDataAPI.get(name, MainASBP.get().getName());
			return data.hasData("askyblockplus-size") ? data.getValue("askyblockplus-size") : "s-0"; 
		}

		@Override
		public void save(String id, UUID player) {
			if (!Configs.sizeUpgrades.containsKey(id)) return;
			String name = Bukkit.getOfflinePlayer(player).getName();
			PlayerData data = PlayerDataAPI.get(name, MainASBP.get().getName());
			data.set("askyblockplus-size", id);
			data.save();
			
			// Luckperms
			User user = LuckPermsProvider.get().getUserManager().getUser(player);
			for (Node node : Lists.newArrayList(user.getNodes())) {
				if (node.getKey().contains("askyblock.island.range."))
				user.data().remove(Node.builder(node.getKey()).build());
			}
			user.data().add(Node.builder("askyblock.island.range." + Configs.sizeUpgrades.get(id).getSize()).build());
			LuckPermsProvider.get().getUserManager().saveUser(user);
		}

		@Override
		public UData getUpgradeData(UUID player) {
			return Configs.sizeUpgrades.get(this.getID(player));
		}

		@Override
		public UData getUpgradeData(String id) {
			return Configs.sizeUpgrades.get(id);
		}
		
	};
	
	public abstract String getID(UUID player);
	public abstract void save(String id, UUID player);
	public abstract UData getUpgradeData(UUID player);
	public abstract UData getUpgradeData(String id);
	
	public void save(UUID player) {
		String id = this.getID(player);
		this.save(id, player);
	}
}
