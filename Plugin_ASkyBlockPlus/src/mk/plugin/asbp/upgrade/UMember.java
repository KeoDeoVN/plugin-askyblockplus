package mk.plugin.asbp.upgrade;

import java.util.List;

import org.bukkit.material.MaterialData;

public class UMember extends UData {
	
	private int member;
	
	public UMember(String next, double price, List<String> desc, int member, MaterialData icon, int slot) {
		super(next, price, desc, icon, slot);
		this.member = member;
	}
	
	public int getMember() {
		return this.member;
	}
	
}
