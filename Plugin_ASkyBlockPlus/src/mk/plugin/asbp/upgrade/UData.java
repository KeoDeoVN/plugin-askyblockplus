package mk.plugin.asbp.upgrade;

import java.util.List;

import org.bukkit.material.MaterialData;

public abstract class UData {
	
	private String next;
	private double price;
	private MaterialData icon;
	private int slot;
	private List<String> desc;
	
	public UData(String next, double price, List<String> desc, MaterialData icon, int slot) {
		this.next = next;
		this.price = price;
		this.desc = desc;
		this.icon = icon;
		this.slot = slot;
	}
	
	public List<String> getDesc() {
		return this.desc;
	}
	
	public String getNext() {
		return this.next;
	}
	
	public double getPrice() {
		return this.price;
	}
	
	public MaterialData getIcon() {
		return this.icon;
	}
	
	public int getSlot() {
		return this.slot;
	}
	
}
