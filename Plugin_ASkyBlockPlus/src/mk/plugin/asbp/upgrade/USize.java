package mk.plugin.asbp.upgrade;

import java.util.List;

import org.bukkit.material.MaterialData;

public class USize extends UData {
	
	private int size;
	
	public USize(String next, double price, List<String> desc, int size, MaterialData icon, int slot) {
		super(next, price, desc, icon, slot);
		this.size = size; 
	}
	
	public int getSize() {
		return this.size;
	}
	
}
