package mk.plugin.asbp.upgrade;

import java.util.List;

import org.bukkit.material.MaterialData;

import mk.plugin.asbp.oregen.Oregen;

public class UOregen extends UData {
	
	private Oregen oregen;
	
	public UOregen(String next, double price, List<String> desc, Oregen oregen, MaterialData icon, int slot) {
		super(next, price, desc, icon, slot);
		this.oregen = oregen;
	}
	
	public Oregen getOregen() {
		return this.oregen;
	}
	
}
