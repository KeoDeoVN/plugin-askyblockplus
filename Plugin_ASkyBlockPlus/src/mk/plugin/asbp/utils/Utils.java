package mk.plugin.asbp.utils;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.wasteofplastic.askyblock.ASkyBlockAPI;
import com.wasteofplastic.askyblock.Island;

public class Utils {
	
	public static boolean isMemberOfIsland(Island is, Player p) {
		return is.getMembers().contains(p.getUniqueId());
	}
	
	public static boolean isMemberOfIsland(Location l, Player p) {
		Island is = ASkyBlockAPI.getInstance().getIslandAt(l);
		if (is == null) return false;
		return isMemberOfIsland(is, p);
	}
	
}
