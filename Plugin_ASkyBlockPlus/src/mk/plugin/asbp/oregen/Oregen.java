package mk.plugin.asbp.oregen;

import java.util.List;

public class Oregen {
	
	private List<OreRate> ores;
	
	public Oregen(List<OreRate> ores) {
		this.ores = ores;
	}
	
	public List<OreRate> getOres() {
		return this.ores;
	}
	
}
