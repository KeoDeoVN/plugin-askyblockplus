package mk.plugin.asbp.oregen;

import org.bukkit.Material;

public class OreRate {
	
	private Material material;
	private double rate;
	
	public OreRate(Material material, double rate) {
		this.material = material;
		this.rate = rate;
	}
	
	public Material getMaterial() {
		return this.material;
	}
	
	public double getRate() {
		return this.rate;
	}
	
}
