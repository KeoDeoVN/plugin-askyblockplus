package mk.plugin.asbp.oregen;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Material;

import com.google.common.collect.Lists;
import com.wasteofplastic.askyblock.Island;

import mk.plugin.asbp.config.Configs;
import mk.plugin.asbp.upgrade.Upgrade;

public class Oregens {
	
	public static Material rate(Island is) {
		
		// Get min oregen
		int minlv = 10;
		for (UUID uuid : is.getMembers()) {
			String id = Upgrade.OREGEN.getID(uuid);
			int lv = Integer.parseInt(id.replace("o-", ""));
			minlv = lv < minlv ? lv : minlv;
		}
		String id = "o-" + minlv;
		
		// Rate
		Oregen oregen = Configs.oregenUpgrades.get(id).getOregen();
		return rate(oregen);
	}
	
	public static Material rate(Oregen oregen) {
		double s = 0;
		List<OreRate> ores = oregen.getOres();
		List<Double> check = Lists.newArrayList();
		for (OreRate ore : ores) {
			s += ore.getRate();
			check.add(s);
		}
		
		double random = random(1, s);
		
		for (int i = 0 ; i < check.size() ; i++) {
			if (check.get(i) >= random) return ores.get(i).getMaterial();
		}
		
		return ores.get(ores.size() - 1).getMaterial();
	}
	
	public static double random(double min, double max) {
		return (new Random().nextInt(new Double((max - min) * 1000).intValue()) + min * 1000) / 1000;
	}
	
}
