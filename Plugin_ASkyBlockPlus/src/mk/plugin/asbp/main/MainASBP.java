package mk.plugin.asbp.main;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import mk.plugin.asbp.command.AdminCommand;
import mk.plugin.asbp.config.Configs;
import mk.plugin.asbp.listener.BlockListener;
import mk.plugin.asbp.listener.InventoryListener;
import mk.plugin.asbp.listener.PlayerListener;
import mk.plugin.asbp.timedfly.TimedFlyTask;

public class MainASBP extends JavaPlugin {
	
	@Override
	public void onEnable() {
		this.saveDefaultConfig();
		this.reloadConfig();
		Configs.reload(this);
		
		Bukkit.getPluginManager().registerEvents(new BlockListener(), this);
		Bukkit.getPluginManager().registerEvents(new InventoryListener(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
		
		this.getCommand("askyblockplus").setExecutor(new AdminCommand());
		
		if (Configs.F_TIMED_FLY) TimedFlyTask.start(this);
	}
	
	public static MainASBP get() {
		return JavaPlugin.getPlugin(MainASBP.class);
	}
	
}
