package mk.plugin.asbp.gui;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import com.google.common.collect.Lists;

import mk.plugin.asbp.upgrade.UData;

public class GUIs {
	

	public static ItemStack getIcon(UData ud) {
		MaterialData icon = ud.getIcon();
		ItemStack is = new ItemStack(icon.getItemType(), 1, (short) icon.getData());
		ItemMeta meta = is.getItemMeta();
		List<String> desc = ud.getDesc();
		List<String> lore = Lists.newArrayList();
		if (desc.size() >= 1) meta.setDisplayName(desc.get(0));
		if (desc.size() > 1) for (int i = 1 ; i < desc.size() ; i++) lore.add(desc.get(i));
		meta.setLore(lore);
		is.setItemMeta(meta);
		return is;
	}
	
	public static ItemStack getBlank() {
		ItemStack is = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName("§e ");
		is.setItemMeta(meta);
		return is;
	}
	
}
