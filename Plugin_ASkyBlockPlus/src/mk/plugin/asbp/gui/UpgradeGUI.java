package mk.plugin.asbp.gui;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

import mk.plugin.asbp.border.Borders;
import mk.plugin.asbp.main.MainASBP;
import mk.plugin.asbp.money.MoneyAPI;
import mk.plugin.asbp.upgrade.UData;
import mk.plugin.asbp.upgrade.Upgrade;

public class UpgradeGUI {
	
	public static void open(Player player) {
		Inventory inv = Bukkit.createInventory(new UG(), 27, "§0§lNÂNG CẤP ĐẢO");
		player.openInventory(inv);
		Bukkit.getScheduler().runTaskAsynchronously(MainASBP.get(), () -> {
			for (int i = 0 ; i < inv.getSize() ; i++) inv.setItem(i, GUIs.getBlank());
			for (Upgrade u : Upgrade.values()) {
				UData ud = u.getUpgradeData(player.getUniqueId());
				inv.setItem(ud.getSlot(), GUIs.getIcon(ud));
			}
		});
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getInventory().getHolder() instanceof UG == false) return;
		e.setCancelled(true);
		if (e.getClickedInventory() != e.getWhoClicked().getOpenInventory().getTopInventory()) return;
		
		// Get upgrade type
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		Upgrade upgrade = null;
		for (Upgrade u : Upgrade.values()) {
			if (u.getUpgradeData(player.getUniqueId()).getSlot() == slot) {
				upgrade = u;
				break;
			}
		}
		if (upgrade == null) return;
		
		// Check level
		UData ud = upgrade.getUpgradeData(player.getUniqueId());
		if (ud.getNext() == null) {
			player.sendMessage("§cNâng cấp đạt tối đa!");
			return;
		}
		UData unext = upgrade.getUpgradeData(ud.getNext());
		
		// Check asset
		double price = unext.getPrice();
		if (!MoneyAPI.moneyCost(player, price)) {
			player.sendMessage("§cBạn cần có " + price + "$ để chi trả");
			return;
		}
		upgrade.save(ud.getNext(), player.getUniqueId());
		
		// Effect
		player.sendMessage("§aNâng cấp thành công");
		player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_LAUNCH, 1, 1);
		open(player);
		
		Borders.check(player, false);
	}
	
}


class UG implements InventoryHolder {
	
	@Override
	public Inventory getInventory() {
		return null;
	}
	
}