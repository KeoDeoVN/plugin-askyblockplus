package mk.plugin.asbp.timedfly;

import java.util.Map;

import org.bukkit.entity.Player;

import com.google.common.collect.Maps;

public class TimedFly {
	
	private static Map<String, Long> expireTime = Maps.newHashMap();
	
	public static boolean isTrigger(Player p) {
		return expireTime.containsKey(p.getName()) && expireTime.get(p.getName()) >= System.currentTimeMillis();
	}
	
	public static boolean check(Player player) {
		if (expireTime.containsKey(player.getName())) {
			if (expireTime.get(player.getName()) < System.currentTimeMillis()) {
				expireTime.remove(player.getName());
				if (!player.hasPermission("askyblockplus.bypass")) {
					player.setFlying(false);
					player.setAllowFlight(false);
					player.sendMessage("§aHết hiệu lực bay");
				}
				return false;
			} return true;
		}
		return false;
	}
	
	public static void set(Player player, long cooldown) {
		expireTime.put(player.getName(), System.currentTimeMillis() + cooldown);
		player.setAllowFlight(true);
		player.setFlying(true);
	}
	
}
