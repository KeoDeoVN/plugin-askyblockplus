package mk.plugin.asbp.timedfly;

import org.bukkit.Bukkit;
import org.bukkit.Particle;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

public class TimedFlyTask extends BukkitRunnable {

	@Override
	public void run() {
		Bukkit.getOnlinePlayers().forEach(player -> {
			if (TimedFly.check(player)) {
				player.spawnParticle(Particle.CLOUD, player.getLocation(), 3, 0.1, 0.1, 0.1, 0.1);
			}
		});
	}	
	
	public static void start(Plugin pl) {
		new TimedFlyTask().runTaskTimer(pl, 0, 5);
	}
	
}
