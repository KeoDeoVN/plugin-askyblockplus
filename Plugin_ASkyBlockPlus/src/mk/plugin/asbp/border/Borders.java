package mk.plugin.asbp.border;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.wasteofplastic.askyblock.ASkyBlockAPI;
import com.wasteofplastic.askyblock.Island;

import mk.plugin.asbp.main.MainASBP;
import mk.plugin.asbp.upgrade.USize;
import mk.plugin.asbp.upgrade.Upgrade;
import net.minecraft.server.v1_12_R1.EntityPlayer;
import net.minecraft.server.v1_12_R1.PacketPlayOutWorldBorder;
import net.minecraft.server.v1_12_R1.PacketPlayOutWorldBorder.EnumWorldBorderAction;
import net.minecraft.server.v1_12_R1.WorldBorder;
import net.minecraft.server.v1_12_R1.WorldServer;

public class Borders {

	public static void check(Player p, boolean async) {
		if (!async) sendBorder(p);
		else {
			Bukkit.getScheduler().runTaskAsynchronously(MainASBP.get(), () -> {
				sendBorder(p);;
			});
		}
	}
	
	public static void sendBorder(Player p) {
		if (p.getWorld() != ASkyBlockAPI.getInstance().getIslandWorld()) return;
		Island is = ASkyBlockAPI.getInstance().getIslandOwnedBy(p.getUniqueId());
		if (is == null) return;
		int x = is.getCenter().getBlockX();
		int z = is.getCenter().getBlockZ();
		double radius = (double) ((USize) Upgrade.SIZE.getUpgradeData(p.getUniqueId())).getSize();
		WorldBorder wb = new WorldBorder();
		wb.setCenter(x, z);
		wb.setSize(radius);
		wb.setWarningDistance(0);
		EntityPlayer player = ((CraftPlayer) p).getHandle();
		wb.world = (WorldServer) player.world;
		player.playerConnection.sendPacket(new PacketPlayOutWorldBorder(wb, EnumWorldBorderAction.SET_SIZE));
		player.playerConnection.sendPacket(new PacketPlayOutWorldBorder(wb, EnumWorldBorderAction.SET_CENTER));
		player.playerConnection.sendPacket(new PacketPlayOutWorldBorder(wb, EnumWorldBorderAction.SET_WARNING_BLOCKS));
		player.playerConnection.sendPacket(new PacketPlayOutWorldBorder(wb, EnumWorldBorderAction.LERP_SIZE));
	}
	
	public static void sendBorder(Player p, double x, double z, double radius) {
		WorldBorder wb = new WorldBorder();
		wb.setCenter(x, z);
		wb.setSize(radius);
		wb.setWarningDistance(0);
		EntityPlayer player = ((CraftPlayer) p).getHandle();
		wb.world = (WorldServer) player.world;
		wb.transitionSizeBetween(radius - 0.2D, radius, 20000000L);
		player.playerConnection.sendPacket(new PacketPlayOutWorldBorder(wb, EnumWorldBorderAction.SET_SIZE));
		player.playerConnection.sendPacket(new PacketPlayOutWorldBorder(wb, EnumWorldBorderAction.SET_CENTER));
		player.playerConnection.sendPacket(new PacketPlayOutWorldBorder(wb, EnumWorldBorderAction.SET_WARNING_BLOCKS));
		player.playerConnection.sendPacket(new PacketPlayOutWorldBorder(wb, EnumWorldBorderAction.LERP_SIZE));
	}

}
