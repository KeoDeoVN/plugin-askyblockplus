package mk.plugin.asbp.config;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.material.MaterialData;
import org.bukkit.plugin.Plugin;

import com.google.common.collect.Maps;

import mk.plugin.asbp.oregen.OreRate;
import mk.plugin.asbp.oregen.Oregen;
import mk.plugin.asbp.upgrade.UMember;
import mk.plugin.asbp.upgrade.UOregen;
import mk.plugin.asbp.upgrade.USize;

public class Configs {
	
	public static boolean F_BORDER = true;
	public static boolean F_TIMED_FLY = true;
	
	public static Map<String, UOregen> oregenUpgrades = Maps.newHashMap();
	public static Map<String, UMember> memberUpgrades = Maps.newHashMap();
	public static Map<String, USize> sizeUpgrades = Maps.newHashMap();
	
	public static void reload(Plugin plugin) {
		try {
			FileConfiguration c = YamlConfiguration.loadConfiguration(new File(plugin.getDataFolder(), "config.yml"));
			F_BORDER = c.getBoolean("feature.island-border");
			F_TIMED_FLY = c.getBoolean("feature.island-timed-fly");
			
			oregenUpgrades.clear();
			memberUpgrades.clear();
			sizeUpgrades.clear();
			
			// Oregen
			File of = new File(plugin.getDataFolder(), "upgrade-oregen.yml");
			if (!of.exists()) {
				FileUtils.copyInputStreamToFile(plugin.getResource("upgrade-oregen.yml"), of);
				of.createNewFile();
			}
			FileConfiguration oc = YamlConfiguration.loadConfiguration(of);
			int oslot = oc.getInt("gui.slot");
			MaterialData oicon = new MaterialData(Material.valueOf(oc.getString("gui.icon").split(":")[0]), Byte.valueOf(oc.getString("gui.icon").split(":")[1]));
			oc.getConfigurationSection("oregen").getKeys(false).forEach(id -> {
				String next = oc.getString("oregen." + id + ".next");
				double price = oc.getDouble("oregen." + id + ".price");
				List<String> desc = oc.getStringList("oregen." + id + ".desc").stream().map(s -> s.replace("&", "§")).collect(Collectors.toList());
				List<OreRate> ores = oc.getStringList("oregen." + id + ".ores").stream().map(s -> new OreRate(Material.valueOf(s.split(":")[0]), Integer.valueOf(s.split(":")[1]))).collect(Collectors.toList());
				Oregen oregen = new Oregen(ores);
				oregenUpgrades.put(id, new UOregen(next, price, desc, oregen, oicon, oslot));
			});
			
			// Size
			File sf = new File(plugin.getDataFolder(), "upgrade-size.yml");
			if (!sf.exists()) {
				FileUtils.copyInputStreamToFile(plugin.getResource("upgrade-size.yml"), sf);
				sf.createNewFile();
			}
			FileConfiguration sc = YamlConfiguration.loadConfiguration(sf);
			int sslot = sc.getInt("gui.slot");
			MaterialData sicon = new MaterialData(Material.valueOf(sc.getString("gui.icon").split(":")[0]), Byte.valueOf(sc.getString("gui.icon").split(":")[1]));
			sc.getConfigurationSection("size").getKeys(false).forEach(id -> {
				String path = "size." + id;
				String next = sc.getString(path + ".next");
				double price = sc.getDouble(path + ".price");
				List<String> desc = sc.getStringList(path + ".desc").stream().map(s -> s.replace("&", "§")).collect(Collectors.toList());
				int size = sc.getInt(path + ".size");
				sizeUpgrades.put(id, new USize(next, price, desc, size, sicon, sslot));
			});
			
			// Member
			File mf = new File(plugin.getDataFolder(), "upgrade-member.yml");
			if (!mf.exists()) {
				FileUtils.copyInputStreamToFile(plugin.getResource("upgrade-member.yml"), mf);
				mf.createNewFile();
			}
			FileConfiguration mc = YamlConfiguration.loadConfiguration(mf);
			int mslot = mc.getInt("gui.slot");
			MaterialData micon = new MaterialData(Material.valueOf(mc.getString("gui.icon").split(":")[0]), Byte.valueOf(mc.getString("gui.icon").split(":")[1]));
			mc.getConfigurationSection("member").getKeys(false).forEach(id -> {
				String path = "member." + id;
				String next = mc.getString(path + ".next");
				double price = mc.getDouble(path + ".price");
				List<String> desc = mc.getStringList(path + ".desc").stream().map(s -> s.replace("&", "§")).collect(Collectors.toList());
				int member = mc.getInt(path + ".member");
				memberUpgrades.put(id, new UMember(next, price, desc, member, micon, mslot));
			});
			
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
